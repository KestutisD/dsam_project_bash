#!/bin/bash
read -p "Enter country number: " country
read -p "Enter probability: " prob


mungy split-2 --skip-header --copy-header $prob ../task_2/data_munging/ctr_by_country/user_country_"$country".csv ../task_2/data_munging/learning/"$country"/train.txt ../task_2/data_munging/learning/"$country"/test.txt
cat ../task_2/data_munging/learning/"$country"/test.txt | tail -n+2 | mpipe extract-column 18 --no-header >> ../task_2/data_munging/learning/"$country"/ytest.txt


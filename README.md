# README #



### What is this repository for? ###

* Data analysis via Linux command line using Vowpal Wabbit

### List of bash scripts ###

Task 1:

* sh task1_split.sh - splitting data into train/validation/test sets
* sh task1_delete_models.sh - delete all existing models for each assignment
* sh task1_tune.sh - tuning model manually (input via command line)
* sh task1_tune_loop.sh - tuning model automatically (input in the existing file)
* sh task1_final_model.sh - testing the final mode/regression equation output

Task 2:

* sh task2_split.sh - splitting data into train/test sets
* sh task_split_country_hours.sh - split by country/hours
* sh task2_model.sh - tuning the logistic regression model and saving metrics to file

Misc:

* sh gitaddcommitpush.sh - upload files to BitBucket
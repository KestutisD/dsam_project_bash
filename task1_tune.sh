#!/bin/bash
read -p "Enter your assigment number: " asg_no
asg='assignment_'$asg_no

read -p "Enter your model name: " model_name
read -p "Passes amount: " pass_no
read -p "Lambda1 level: " lambda1
read -p "Lambda2 level: " lambda2


mungy csv2vw ../task_1/"$asg"/data_munging/train.txt --delim=' ' --label=0 --no-binary-label --no-header | vw --final_regressor ../task_1/"$asg"/vw_serialized_models/"$model_name".vw_model --readable_model ../task_1/"$asg"/vw_readable_models/"$model_name".readable_model --loss_function squared --passes "$pass_no" --holdout_off -q nn --cache --l1 "$lambda1" --l2 "$lambda2" --normalized 

cat ../task_1/"$asg"/vw_readable_models/"$model_name".readable_model


rm ./.cache; mungy csv2vw ../task_1/"$asg"/data_munging/validation.txt --delim=' ' --label=0 --no-binary-label --no-header | vw --initial_regressor ../task_1/"$asg"/vw_serialized_models/"$model_name".vw_model --invert_hash ../task_1/"$asg"/vw_inverted_hashes/"$model_name".invert_hash --predictions ../task_1/"$asg"/vw_predictions/"$model_name".txt --normalized --testonly

paste -d ' ' ../task_1/"$asg"/vw_predictions/"$model_name".txt ../task_1/"$asg"/data_munging/yvalidation.txt > ../task_1/"$asg"/data_munging/"$model_name"_predictions.txt

echo "Passes = $pass_no, L1 = $lambda1, L2= $lambda2" >> metrics/"$asg"_metrics.txt
mpipe metrics-reg --has-r2 ../task_1/"$asg"/data_munging/"$model_name"_predictions.txt
mpipe metrics-reg --has-r2 ../task_1/"$asg"/data_munging/"$model_name"_predictions.txt >> metrics/"$asg"_metrics.txt

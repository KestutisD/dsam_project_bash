#!/bin/bash 
asg='assignment_1'

mungy csv2vw ../task_1/"$asg"/data_munging/train.txt --delim=' ' --label=0 --no-binary-label --no-header | vw --final_regressor ../task_1/"$asg"/vw_serialized_models/final_model.vw_model --readable_model ../task_1/"$asg"/vw_readable_models/final_model.readable_model --loss_function squared --passes 5 --holdout_off -q nn --cache --l1 0.00005 --l2 0.00005 --normalized 

rm ./.cache

mungy csv2vw ../task_1/"$asg"/data_munging/validation.txt --delim=' ' --label=0 --no-binary-label --no-header | vw --initial_regressor ../task_1/"$asg"/vw_serialized_models/final_model.vw_model --invert_hash ../task_1/"$asg"/vw_inverted_hashes/final_model.invert_hash --predictions ../task_1/"$asg"/vw_predictions/final_model.txt --normalized --testonly
paste -d ' ' ../task_1/"$asg"/vw_predictions/final_model.txt ../task_1/"$asg"/data_munging/yvalidation.txt > ../task_1/"$asg"/data_munging/final_model_predictions.txt
mpipe metrics-reg --has-r2 ../task_1/"$asg"/data_munging/final_model_predictions.txt

mungy csv2vw ../task_1/"$asg"/data_munging/test.txt --delim=' ' --label=0 --no-binary-label --no-header | vw --initial_regressor ../task_1/"$asg"/vw_serialized_models/final_model.vw_model --invert_hash ../task_1/"$asg"/vw_inverted_hashes/final_model.invert_hash --predictions ../task_1/"$asg"/vw_predictions/final_model.txt --normalized --testonly
paste -d ' ' ../task_1/"$asg"/vw_predictions/final_model.txt ../task_1/"$asg"/data_munging/ytest.txt > ../task_1/"$asg"/data_munging/final_model_predictions.txt
mpipe metrics-reg --has-r2 ../task_1/"$asg"/data_munging/final_model_predictions.txt

cat ../task_1/"$asg"/vw_inverted_hashes/final_model.invert_hash

#!/bin/bash 

country=840

for lambda1 in 0.001 0.0001 0.00001
do
for lambda2 in 0.001 0.0001 0.00001
do

mungy csv2vw ../task_2/data_munging/learning/"$country"/train.txt --label=impression_was_clicked --binary-label --header --all-categorical --ignored-columns=log_time --nested-val-delims=";," | vw --normalized --link=logistic --final_regressor ../task_2/modelling/models/model_test.vw --predictions ../task_2/modelling/predictions/predictions_test.txt --l1 $lambda1 --l2 $lambda2

mungy csv2vw ../task_2/data_munging/learning/"$country"/test.txt --label=impression_was_clicked --binary-label --header --all-categorical --ignored-columns=log_time --nested-val-delims=";," | vw --normalized --link=logistic  --initial_regressor ../task_2/modelling/models/model_test.vw --predictions ../task_2/modelling/predictions/predictions_test.txt --testonly

echo "L1 = $lambda1, L2= $lambda2" >> metrics/task2_"$country"_l1l2.txt
cat ../task_2/data_munging/learning/"$country"/test.txt | tail -n+2 | mpipe extract-column 18 --no-header --delim="\t" | paste -d " " - ../task_2/modelling/predictions/predictions_test.txt | mpipe metrics-cls >> metrics/task2_"$country"_l1l2.txt

done
done
exit 0
